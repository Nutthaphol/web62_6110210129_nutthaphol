﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages
{
    public class IndexModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public IndexModel(NutDrugs.Data.NutDrugsContext  context)
        {
            _context = context;
        }

        public IList<Drugs> Drugs { get;set; }
        public IList<TypeDrugs> TypeDrugs { get;set; }


        public string Owner {get;set;}

        public async Task OnGetAsync()
        {
            Owner = "นายแพทย์ ณัฐพล เดชประมวลพล";
            
            Drugs = await _context.Drugse
                .Include(t => t.TypeDrugs).ToListAsync();

            TypeDrugs = await _context.TypeDrugse.ToListAsync();

        }    
    }
}
