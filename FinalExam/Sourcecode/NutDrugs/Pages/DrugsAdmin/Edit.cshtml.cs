using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.DrugsAdmin
{
    public class EditModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public EditModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Drugs Drugs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drugs = await _context.Drugse
                .Include(d => d.TypeDrugs)
                .Include(d => d.UserAdmin).FirstOrDefaultAsync(m => m.DrugsID == id);

            if (Drugs == null)
            {
                return NotFound();
            }
           ViewData["TypeDrugsID"] = new SelectList(_context.TypeDrugse, "TypeDrugsID", "DrugsType");
           ViewData["UserId"] = new SelectList(_context.Users, "Id", "FirstName");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Drugs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DrugsExists(Drugs.DrugsID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool DrugsExists(int id)
        {
            return _context.Drugse.Any(e => e.DrugsID == id);
        }
    }
}
