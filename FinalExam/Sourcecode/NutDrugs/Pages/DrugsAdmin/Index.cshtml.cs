using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.DrugsAdmin
{
    public class IndexModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public IndexModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        public IList<Drugs> Drugs { get;set; }

        public async Task OnGetAsync()
        {
            Drugs = await _context.Drugse
                .Include(d => d.TypeDrugs)
                .Include(d => d.UserAdmin).ToListAsync();
        }
    }
}
