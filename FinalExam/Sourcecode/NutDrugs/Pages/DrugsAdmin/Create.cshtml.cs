using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.DrugsAdmin
{
    public class CreateModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public CreateModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["TypeDrugsID"] = new SelectList(_context.TypeDrugse, "TypeDrugsID", "DrugsType");
        ViewData["UserId"] = new SelectList(_context.Users, "Id", "FirstName");
            return Page();
        }

        [BindProperty]
        public Drugs Drugs { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Drugse.Add(Drugs);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}