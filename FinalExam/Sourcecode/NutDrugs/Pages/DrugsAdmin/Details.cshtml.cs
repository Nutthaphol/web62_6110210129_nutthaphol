using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.DrugsAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public DetailsModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        public Drugs Drugs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drugs = await _context.Drugse
                .Include(d => d.TypeDrugs)
                .Include(d => d.UserAdmin).FirstOrDefaultAsync(m => m.DrugsID == id);

            if (Drugs == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
