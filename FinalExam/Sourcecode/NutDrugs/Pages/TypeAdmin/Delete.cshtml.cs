using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.TypeAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public DeleteModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TypeDrugs TypeDrugs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeDrugs = await _context.TypeDrugse.FirstOrDefaultAsync(m => m.TypeDrugsID == id);

            if (TypeDrugs == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeDrugs = await _context.TypeDrugse.FindAsync(id);

            if (TypeDrugs != null)
            {
                _context.TypeDrugse.Remove(TypeDrugs);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
