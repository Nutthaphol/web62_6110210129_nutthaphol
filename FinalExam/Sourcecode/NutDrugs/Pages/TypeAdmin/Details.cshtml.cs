using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.TypeAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public DetailsModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        public TypeDrugs TypeDrugs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeDrugs = await _context.TypeDrugse.FirstOrDefaultAsync(m => m.TypeDrugsID == id);

            if (TypeDrugs == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
