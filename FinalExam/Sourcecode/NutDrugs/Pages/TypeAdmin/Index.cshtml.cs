using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.TypeAdmin
{
    public class IndexModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public IndexModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        public IList<TypeDrugs> TypeDrugs { get;set; }

        public async Task OnGetAsync()
        {
            TypeDrugs = await _context.TypeDrugse.ToListAsync();
        }
    }
}
