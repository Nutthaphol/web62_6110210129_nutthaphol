using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.TypeAdmin
{
    public class CreateModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public CreateModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public TypeDrugs TypeDrugs { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.TypeDrugse.Add(TypeDrugs);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}