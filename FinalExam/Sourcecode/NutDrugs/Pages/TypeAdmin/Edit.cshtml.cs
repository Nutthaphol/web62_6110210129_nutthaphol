using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NutDrugs.Data;
using NutDrugs.Models;

namespace NutDrugs.Pages.TypeAdmin
{
    public class EditModel : PageModel
    {
        private readonly NutDrugs.Data.NutDrugsContext _context;

        public EditModel(NutDrugs.Data.NutDrugsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TypeDrugs TypeDrugs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TypeDrugs = await _context.TypeDrugse.FirstOrDefaultAsync(m => m.TypeDrugsID == id);

            if (TypeDrugs == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(TypeDrugs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeDrugsExists(TypeDrugs.TypeDrugsID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TypeDrugsExists(int id)
        {
            return _context.TypeDrugse.Any(e => e.TypeDrugsID == id);
        }
    }
}
