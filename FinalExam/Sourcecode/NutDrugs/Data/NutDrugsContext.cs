using NutDrugs.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace NutDrugs.Data
{
    public class NutDrugsContext : IdentityDbContext<User>
    {
        public DbSet<Drugs> Drugse {get; set;}
        public DbSet<TypeDrugs> TypeDrugse {get; set;}

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=Drugs.db");
        }
    }
}