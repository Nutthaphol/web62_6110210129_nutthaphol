using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace NutDrugs.Models
{
    public class User : IdentityUser{
        public string FirstName { get; set;}
        public string LastName { get; set;}
    }

    public class TypeDrugs 
    {
        public int TypeDrugsID {get; set;}
        public string DrugsType {get; set;}
    }


    public class Drugs
    {
        public int DrugsID { get; set; }

        public string UserId {get; set;}
        public User UserAdmin {get; set;}

        public int TypeDrugsID { get; set; }
        public TypeDrugs TypeDrugs { get; set; }

        public string DrugsName { get; set; }
        public int DrugsNumber { get; set; }
        public double price { get; set; }
        public string unit { get; set; }
    }
}